public with sharing class Utils {
    @AuraEnabled
    public static void getList() {
        String roleId = null;
        List<Access__c> accessList = new List<Access__c>([SELECT Id, Role__c FROM Access__c 
                                            WHERE User__c =: UserInfo.getUserId() LIMIT 1]);
        if(!accessList.isEmpty()){
            roleId = accessList[0].Role__c;
            String query = 'SELECT ';
            Set<String> fields = new Set<String>(Role__c.sObjectType.getDescribe().fields.getMap().keySet());
            for(String field : fields) {
                query += field+', ';
            }

            query =  query.substring(0, query.length()-2);
            query += ' FROM Role__c WHERE Id =\''+roleId+'\'';
            
            System.debug('Query'+query);
            List<Role__c> sobjList = Database.query(query);
            System.debug('sobjList'+sobjList);
        }
    }
}